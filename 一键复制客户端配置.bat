@echo off

setlocal EnableDelayedExpansion

set ClientDir=.\dist\client\

If Defined Client_Cfg_Dir (
	set ClientDir=%Client_Cfg_Dir%
	Echo 已配置服务器目标拷贝路径环境变量Server_Cfg_Dir,配置拷贝目标目录 !ClientDir! 
) Else (
	Echo 未配置服务器目标拷贝路径环境变量Server_Cfg_Dir,配置拷贝目标目录 !ClientDir! 
)

rem 复制配置到scene
xcopy .\config\client\*.json !ClientDir!\config\ /y

echo 复制完毕


pause