@echo off

setlocal EnableDelayedExpansion

set ServerDir=.\dist\server\

If Defined Server_Cfg_Dir (
	set ServerDir=%Server_Cfg_Dir%
	Echo 已配置服务器目标拷贝路径环境变量Server_Cfg_Dir,配置拷贝目标目录 !ServerDir! 
) Else (
	Echo 未配置服务器目标拷贝路径环境变量Server_Cfg_Dir,配置拷贝目标目录 !ServerDir! 
)

rem 复制配置到scene
xcopy .\config\server\*.lua !ServerDir!\config\ /y

echo 复制完毕


pause